# Systems Design

## Client Server Model

The client sends data to the server, and the server sends data back to the client. The client does not know what the server represents.  Behind the scenes, the client makes a DNS query to find the IP address of the server.  Once it has the IP address, it can make a connection to the server to request the data that it needs.

Clients can send information in the form of bytes to an IP address. The IP address of the server can be retrieved from a DNS provider (Google Cloud Platform, Azure, AWS, etc). The IP address is essentially the endpoint where your HTTP requests terminate.

```bash
dig {sitename.domain}
```

Entering the command above into your terminal will provide you with the results of a DNS query, which will give you the requested IP address of a website URL.

When the client sends data in the form of bytes to the IP address given from the DNS server, it uses HTTP.  HTTP is a protocol through which servers know how to and in what format to send information to the return IP address.

The bytes (characters) that are sent to the server over HTTP are transferred in packets in some special format.  The packet also contains the source IP address of the packet.  The source IP address is where the server will send the response packet.

A server listens to requests on specific ports.  You must specify which port on which you want to communicate. Most clients know which ports to use based on the protocol.

Basic ports

```bash
80: HTTP
443: HTTPS
22: SSH
53: DNS lookup
```

