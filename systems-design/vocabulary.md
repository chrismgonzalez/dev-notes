# Vocabulary

## Client-Server Model

#### Client

A machine or process that requests data or service from a server.
#### Server

A machine or process that provides data or service for a client, usually by listening for incoming network calls

#### Client-Server Model

The paradigm by which modern systems are designed, which consists of clients requesting data or service from servers and servers providing data or service to clients

#### IP Address

An address given to each machine connected to the public internet. IPv4 addresses consist of four numbers separated by dots: a.b.c.d where all four numbers are between 0 and 255.

```bash
127.0.0.1: Your own local machine.  Referred to as localhost.

192.168.x.y: Your private network. For instance, your machine and all machines on your private wifi network will usually have the 192.168 prefix
```

#### Port

In order for multiple programs to listen for new network connections on the same machine without colliding, they pick a port to listen on.  A port is an integer between 0 and 65,535.

Typically ports 0-1023 are reserved for system ports and should not be used.  Certain ports have predefined uses, and although you usually won't be required to have them memorized, they can still come in handy

#### DNS

Short for Domain Name System, it describes the entities and protocols involved in the translation from domain names to IP addresses.  Typically machines make a DNS query to a well known entity which is responsible for return the IP address (or multiple) of the requested domain name in the response.